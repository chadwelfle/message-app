#Message App Read Me File
---
##Clone application from bitbucket
From the command line run the following command `git clone https://chadwelfle@bitbucket.org/chadwelfle/message-app.git`

##Start application
From the directory that the cloned repo was saved too run `npm install` to install all the libraries needed to run the app

From the command line run `npm start` from the same directory above.  This will fire up the application and you will be able to use it by going to 

###Storage
For this simple app I am taking advantage of mLab (https://mlab.com) so that we dont need to set up a local instance of mongo.  I chose mongo because that is what I am familar with and like the way that the data is structured and returned.  This was a simple enough way to keep track of data