const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let MessageSchema = new Schema({
    message: {type: String, required: true, max: 500},
    user: {type: objectId, ref: 'User'},
    date:  {type: Date, default: Date.now},
    _id: Schema.Types.ObjectId
})

module.exports = mongoose.model('Message', MessageSchema);